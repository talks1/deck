import React from 'react'
import { Helmet } from 'react-helmet'
import { ThemeProvider, merge } from 'theme-ui'
import split from './split-slides'
import { Context } from './context'
import Keyboard from './keyboard'
import modes from './modes'
import Storage from './storage'
import Container from './container'
import Slide from './slide'
import baseTheme from './theme'
import { AnimationState, options as animationOptions, handleChange as animationChange } from './animations'
import { ThemeState } from './components'

const getIndex = props => {
  if (!props.location) return 0
  const n = Number(props.location.hash.replace(/^#/, ''))
  return n
}

export default props => {
  const slides = split(props)

  const { animationMode } = props
  if(animationMode){
    let mode = animationOptions.find(x=>x.label === animationMode)
    //console.log(mode)
    if(mode)
      animationChange(mode)
  }

  // console.log(AnimatedInDiv)
  // console.log(AnimatedOutDiv)
 
  //console.log(`slides: ${slides}`)
  const [index, setIndex] = React.useState(getIndex(props))
  const { slug } = props.pageContext || {}
  const slide = slides[index]


  const [mode, setMode] = React.useState(modes.default)
  const toggleMode = next => setMode(current =>
    current === next ? modes.default : next
  )

  const [step, setStep] = React.useState(0)
  const [steps, setSteps] = React.useState(0)

  const [transition,setTransition] = React.useState(1)

  const lastIndex = React.useRef(0)
  const direction = index - lastIndex.current

  const lastSlide = slides[lastIndex.current]

  let AnimatedInDiv = AnimationState.in
  let AnimatedOutDiv = AnimationState.out

  React.useEffect(() => {
    lastIndex.current = index
  }, [index])

  React.useEffect(() => {
    if (props.location.pathname === '/print') return
    props.navigate('/#' + index, {
      replace: true,
    })
  }, [index])

  React.useEffect(() => {
    if (props.location.pathname === '/print') {
      setMode(modes.print)
    }
    if (!slide) {
      props.navigate('/')
      setIndex(0)
    }
  }, [])

  if (!slide) return false

  const context = {
    slides,
    slug,
    index,
    setIndex,
    direction,
    length: slides.length,
    slide,
    mode,
    setMode,
    toggleMode,
    notes: slide.notes,
    header: slides.header,
    footer: slides.footer,
    step,
    setStep,
    steps,
    setSteps,
  }

  context.previous = () => {
    if (steps && step > 0) {
      setStep(n => n - 1)
    } else {
      setIndex(n => n > 0 ? n - 1 : n)
      setStep(0)
      setSteps(0)
    }
  }

  context.next = () => {
    if (step < steps) {
      setStep(n => n + 1)
    } else {
      setTransition(-1)
      setTimeout(()=>{
        setTransition(1)
        setIndex(n => n < slides.length - 1 ? n + 1 : n)
        setStep(0)
        setSteps(0)
      },1000)
      
    }
  }

  //console.log(ThemeState.name)
  //console.log(props.theme )
  //const theme = merge(baseTheme, props.theme || {})
  const theme = merge(baseTheme, props.theme || ThemeState.state || {})

  // React.useEffect(()=>{
  //   AnimatedDiv = animatedDiv()
  // },[])

  return (
    <Context.Provider value={context}>
      <Keyboard />
      <Storage />
      <Helmet>
        {slides.head.children}
        {theme.googleFont && <link rel='stylesheet' href={theme.googleFont} />}
      </Helmet>
      <ThemeProvider
        theme={theme}
        components={theme.components}>
        <Container>{
          transition===1 ?
            <Slide>
              <AnimatedInDiv>
                {slide}
              </AnimatedInDiv>
            </Slide> :
            <Slide>
            <AnimatedOutDiv>
              {slide}
            </AnimatedOutDiv>
          </Slide>
        }
        </Container>
      </ThemeProvider>
    </Context.Provider>
  )
}
