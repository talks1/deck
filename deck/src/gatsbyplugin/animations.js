import React from 'react'
import styled, { keyframes } from 'styled-components';
import { bounce, fadeIn, fadeOut, flipInX, slideInRight, slideOutLeft , rotateInDownLeft, rotateOutUpRight } from 'react-animations';
 
// const bounceAnimation = keyframes`${bounce}`;
//  const bouncyDiv = ()=>styled.div`animation: 1s ${bounceAnimation};`


// const flipInAnimation = keyframes`${flipInX}`;
// const flipInDiv = ()=>styled.div`animation: 5s ${flipInAnimation};`

export const NoneDiv = styled.div``

const fadeInAnimation = keyframes`${fadeIn}`; 
export const FadeInDiv = styled.div`animation: 2s ${fadeInAnimation};`

const fadeOutAnimation = keyframes`${fadeOut}`; 
export const FadeOutDiv = styled.div`animation: 2s ${fadeOutAnimation};`

const rotateInDownLeftAnimation = keyframes`${rotateInDownLeft}`;
export const RotateInDiv = styled.div`animation: 2s ${rotateInDownLeftAnimation};`

const rotateOutUpRightAnimation = keyframes`${rotateOutUpRight}`;
export const RotateOutDiv = styled.div`animation: 2s ${rotateOutUpRightAnimation};`

const slideInAnimation = keyframes`${slideInRight}`;
export const SlideInDiv = styled.div`animation: 2s ${slideInAnimation};`

const slideOutAnimation = keyframes`${slideOutLeft}`;
export const SlideOutDiv = styled.div`animation: 2s ${slideOutAnimation};`

export const AnimationState = {
    in: NoneDiv,
    //in: RotateInDiv,
    out: NoneDiv,
    //out: RotateOutDiv,
    label: 'None'
}

export const options = [
    { in: NoneDiv, out: NoneDiv, label: 'none' },
    { in: SlideInDiv, out: SlideOutDiv, label: 'slide' },
    { in: FadeInDiv, out: FadeOutDiv, label: 'fade' },
    { in: RotateInDiv, out: RotateOutDiv, label: 'rotate' },
];

export const handleChange = (selection)=>{
  //console.log(`handleChange ${selection}`)
  //console.log(selection)
  AnimationState.in = selection.in
  AnimationState.out = selection.out
  AnimationState.label = selection.label
}
