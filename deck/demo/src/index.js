import React from 'react';
import { render} from 'react-dom';

import * as themes from '../../src/themes/index.js'
import { components }  from '../../src/gatsbyplugin/index.js'

import testtheme from './testtheme'

import Content from './index.md'
const { SlideDeck } = components

const App = (props) => {
    return  <SlideDeck
                location={location}
                theme={testtheme}
                //theme={themes.comic}
                navigate={()=>{}}
                components={components}
                //animationMode='rotate'
                children={Content({components}).props.children}
            >
            </SlideDeck>
}

render(<App />, document.querySelector('#presentation'));

