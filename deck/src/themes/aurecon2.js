const white = '#ffffff'
const black = '#11111f'
const blue = '#2d5dd7'
const green = '338833'
const gray ='CCCCCC'


export default {
  fonts: {
    body: '"Crimson Text", serif',
  },
  googleFont: 'https://fonts.googleapis.com/css?family=Crimson+Text',
  colors: {
    text: black,
    background: gray,
    link: black,
    title: green,
    menu: black
  },
  styles: {
    root: {
      height: 'auto'
    },
    h1: {
      color: 'title',
      margin: '0px 0px 0px 0px'
    },
    h2: {
      fontSize: '1.25em'
    },
    h3: {
      fontSize: '1em'
    },
    hr: {
      paddingTop: '30px',
      "border-bottom": '4px solid'
    },
    Image: {
        
    },
    Header: {
      color: 'white',
      height: '50px',
      margin: '0px 0px 0px 0px',
      bg: 'title',
      display: 'inline-block'
    },
    Footer: {
      color: 'white',
      height: '50px',
      margin: '0px 0px 0px 0px',
      bg: 'title'
    },
    root: {
      textAlign: 'left',
    },
    Split: {
      height: 'none'
    },
    Slide: {
        overflow: 'scroll',
        display: 'block',
        padding: '1em',
        //textAlign: 'center',
        height: 'auto',
        paddingBottom: '100px',
        paddingLeft: '100px'
    },
  },
}
